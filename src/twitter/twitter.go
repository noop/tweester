package twitter

import (
	"github.com/ChimeraCoder/anaconda"
	"log"
	"strings"
)

/* A configuration file to hold twitter related info */
type Twitter struct {
	Username             string
	Password             string
	Application_name     string
	Consumer_key         string
	Consumer_secret      string
	Access_token         string
	Access_token_secret  string
	Post_volume_in_sec   int32
	Post_frequency_range int32
	Banned_words         []string
	Trigger_words        []string
	Twitter_api          anaconda.TwitterApi
}

/* Make a twatter object */
var twitter Twitter

/**
* Setup the twitter api noise
 */
//func (t *Twitter) Auth() {
//	    /* Configure the anaconda client */
//          anaconda.SetConsumerKey(twitter.Consumer_key)
//          anaconda.SetConsumerSecret(twitter.Consumer_secret)
//	    t.Twitter_api = anaconda.NewTwitterApi(twitter.Access_token, twitter.Access_token_secret)
//	    log.Println("Twitter auth info [ ",t.Twitter_api,"]")
//}

/**
*   Supply the body to tweet
 */
func (t *Twitter) Tweet(content string) bool {
	tweeted := false
	tweet, err := t.Twitter_api.PostTweet(content, nil)
	if err != nil {
		if strings.Contains(err.Error(), "duplicate") {
			log.Println("Couldn't Tweet [ ", content, "], error [ ", err, " ]")
		} else {
			log.Fatal("Received a non-duplicate tweet error, tweet [ ", content, " ], error [ ", err, " ]")
		}
	} else {
		tweeted = true
		log.Println("Tweet [ ", content, " ] Tweet length [ ", len(tweet.Text), "]")
	}
	return tweeted
}
