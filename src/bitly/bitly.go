package bitly

import (
	"errors"
	"helpers"
	"html"
	"io/ioutil"
	"log"
	"net/http"
)

type Bitly struct {
	Username     string
	Password     string
	Api_key      string
	Access_token string
	Base_url     string
	Configured   bool
}

var client interface{}

func (b *Bitly) Auth() {
	client := &http.Client{}

	req, err := http.NewRequest("POST", "https://api-ssl.bitly.com/oauth/access_token", nil)
	if err != nil {
		log.Fatal(err)
	}

	// Set the Basic auth info in the header
	req.SetBasicAuth(b.Username, b.Password)

	resp, err := client.Do(req)
	if err != nil {
		log.Fatal("Unable to auth with Bitly, err [ ", err, " ]")
	}
	access_token_b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	b.Access_token = string(access_token_b)
	b.Configured = true
}

func (b *Bitly) Shorten(url string) (string, error) {
	escaped_url := html.EscapeString(url)
	if b.Access_token == "" {
		return "", errors.New("Access token was nil, authenticate with Bit.ly first!")
	}
	bitly_url := b.Base_url + "/v3/shorten?access_token=" + b.Access_token + "&longUrl=" + escaped_url
	resp, err := http.Get(bitly_url)
	if err != nil {
		return "", err
	}
	// the bitly response is json
	bitly_response, err := helpers.Io_parse_json(resp.Body)
	if err != nil {
		return "", err
	}
	shortened_url := bitly_response["data"].(map[string]interface{})["url"].(string)

	return shortened_url, nil
}
