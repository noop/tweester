package main

import (
	"config"
	"encoding/json"
	"flag"
	"github.com/ChimeraCoder/anaconda"
	"helpers"
	"log"
	"math/rand"
	"net/http"
	"os"
	"sync"
	"time"
	"twitter"
	"youtube"
)

type CommentLink struct {
	Comment string
	Url     string
	Valid   bool
}

func Addmap(a map[string]string, b map[string]string) map[string]string {
	var newMap = make(map[string]string)
	for k, v := range b {
		newMap[k] = v
	}
	for k, v := range a {
		newMap[k] = v
	}
	return newMap
}

/* constants */
var TWEET_LENGTH = 140

/* Our root config object */
var Config = new(config.Config)

/**
* Setup the twitter api noise
 */
func TwitterAuth(twitter twitter.Twitter) anaconda.TwitterApi {
	/* Configure the anaconda client */
	anaconda.SetConsumerKey(twitter.Consumer_key)
	anaconda.SetConsumerSecret(twitter.Consumer_secret)
	return *anaconda.NewTwitterApi(twitter.Access_token, twitter.Access_token_secret)
}

/**
* Retrieve from YouTube the top ranked videos
 */
func get_video_urls(top_videos string) ([]string, error) {
	var top_video_urls []string
	log.Println("Youtube video URL [ ", top_videos, " ]")
	resp, err := http.Get(top_videos)

	if err != nil {
		return nil, err
	}

	/* Create a youtube response struct */
	video := new(youtube.Response)

	/* Decode the provided response body */
	dec := json.NewDecoder(resp.Body)
	err = dec.Decode(&video)

	/* Close the response body */
	resp.Body.Close()

	if err != nil {
		log.Fatal("Unable to read resp body, err [ ", err, " ]")
	}

	feed := video.Feed
	for _, entry := range feed.Entry {
		/* Traverse the feed object to see if we have a valid Href */
		if entry.GdComments.GdfeedLink.Href != "" {
			log.Println("href [ ", entry.GdComments.GdfeedLink.Href, " ]")
			top_video_urls = append(top_video_urls, entry.GdComments.GdfeedLink.Href)
		}
	}
	return top_video_urls, nil
}

/**
* Retrieve the latest comments from the video url
 */
func get_comments(video_url string, c chan<- map[string]string) {
	comment_collection := make(map[string]string)
	resp, err := http.Get(video_url)

	if err != nil {
		log.Fatal(err)
	}
	comments := new(youtube.Response)
	dec := json.NewDecoder(resp.Body)
	err = dec.Decode(&comments)

	comment_counter := 0
	if err == nil {
		for _, entry := range comments.Feed.Entry {
			comment := entry.Content.T
			video_url := entry.Link[1].Href
			if len(comment) <= 100 && len(comment) >= 10 {
				// We don't care if we crush the previous comment, it will be a dupe anyway
				comment_collection[comment] = video_url
				comment_counter++
			}
		}
	}
	log.Println("Url [ ", video_url, " ] Comment counter [ ", comment_counter, " ]")
	c <- comment_collection
}

/* Retrieve the latest comment set from series of URLs */
func get_video_comments(video_urls []string, url_ext string) []CommentLink {
	/* A channel to transmit the comment maps */
	var comments_chan = make(chan map[string]string, 100)

	/* A WaitGroup to control processing of the comments */
	var comments_wg sync.WaitGroup

	/* Retrieve the comments for the top ten youtube urls */
	total_threads := 0
	for _, url := range video_urls {
		/* Setup the WaitGroup to wait for the goroutines below to be done */
		comments_wg.Add(1)
		total_threads++

		/* Anonymous function to start goroutines to pull in all the comments at once */
		go func(url string, comments_chan chan<- map[string]string) {
			defer comments_wg.Done()
			url += url_ext
			log.Println("Getting comments for URL [ ", url, " ] ")
			get_comments(url, comments_chan)
		}(url, comments_chan)
	}

	log.Println("Waiting for the goroutines to return...")
	comments_wg.Wait()

	/* Retrieve all the comments */
	var comments_map = make(map[string]string)

	counter := 0
	log.Println("Goroutines have returned, reading the channel...")
	for p_comms := range comments_chan {
		counter++
		comments_map = Addmap(comments_map, p_comms)
		/* Tear down the comments channel so that the for loop ends */
		if counter >= total_threads {
			close(comments_chan)
		}
	}
	log.Println("Total comments [ ", len(comments_map), " ]")

	/* Convert the map contents into  []CommentLink */
	var comments []CommentLink
	for comment, url := range comments_map {
		var commentlink CommentLink

		commentlink.Url = url
		commentlink.Comment = comment
		commentlink.Valid = true

		comments = append(comments, commentlink)
	}
	return comments
}

func get_processed_comments(commentLinks []CommentLink, trigger_words []string, banned_words []string) []CommentLink {
	/* A WaitGroup */
	var comments_wg sync.WaitGroup

	/* Process the retrieved comments to make the Twitter friendly */
	comment_count := 0
	commentLink_chan := make(chan CommentLink, 100)
	for _, commentlink := range commentLinks {
		/* Parallize the processing of the comments */
		comments_wg.Add(1)
		comment_count++
		go func(comment string, video_url string, comments_chan chan<- CommentLink) {
			defer comments_wg.Done()
			processed_comment, err := helpers.ProcessComment(comment, trigger_words, banned_words)
			var tweet_content CommentLink

			/* Mark the message as valid or not */
			if err != nil {
				log.Println("Triggered error [ ", err, " ]")
				tweet_content.Valid = false
				/* Nothing to do, return */
				return
			}

			/* Construct a CommentLink pairing to pass around */
			tweet_content.Comment = processed_comment
			tweet_content.Url = video_url
			tweet_content.Valid = true

			/* Post the tweet contents to the channel */
			comments_chan <- tweet_content
		}(commentlink.Comment, commentlink.Url, commentLink_chan)
	}
	comments_wg.Wait()

	/* Collect the CommentLinks */
	comment_link_counter := 0
	var processed_comments []CommentLink
	for comment_link := range commentLink_chan {
		comment_link_counter++
		/* If the comment link is valid, add it to the set of comment links */
		if comment_link.Valid {
			processed_comments = append(processed_comments, comment_link)
		}
		/* Tear down the comment channel so that the for loop ends */
		if comment_link_counter >= comment_count {
			close(commentLink_chan)
		}
	}
	return processed_comments
}

/* Format the tweet with the bitly link */
func format_tweet(comment_link CommentLink) (tweet_content string) {
	/* If bitly is configured, shorten the URL */
	bitly := Config.Bitly
	log.Println("Bitly configured? [ ", bitly.Configured, " ]")
	if bitly.Configured {
		bitly_url, err := bitly.Shorten(comment_link.Url)
		log.Println("Bitly Url [ ", bitly_url, " ]")
		if err != nil {
			log.Println("Bitly error [ ", err, " ]")
			return comment_link.Comment
		}

		/* Check that our contents can fit into a tweet */
		if len(comment_link.Comment) > TWEET_LENGTH-len(bitly_url) {
			tweet_content = comment_link.Comment[0:(TWEET_LENGTH-len(bitly_url))] + " " + bitly_url
		} else {
			tweet_content = comment_link.Comment + " " + bitly_url
		}
	}
	return tweet_content
}

func main() {
	var config_path = flag.String("config", "config/config.json", "Specific the path to a config file")
	flag.Parse()

	config_file, err := os.Open(*config_path)

	if err != nil {
		log.Println(err)
	}

	/* Build the config object */
	Config.Build(config_file)

	/* Setup the general config objects */
	youtube_config := Config.Youtube
	twitter_config := Config.Twitter
	db_config := Config.Db_handler

	/* Configure the anaconda client */
	twitter_config.Twitter_api = TwitterAuth(twitter_config)

	log.Println("Let's party and decode some Youtube JSON responses!")

	/* Main loop, do this forever! */
	for {
		/* Retrieve the top ten videos */
		top_urls, err := get_video_urls(youtube_config.Base_url + youtube_config.Video_ext)
		if err != nil {
			log.Println(err)
			continue
		}

		/* Retrieve the requests comments associated with urls */
		log.Println("Retrieving the video comments ...")
		commentLinks := get_video_comments(top_urls, youtube_config.Comment_ext)
		log.Println("Done retrieving the video comments ...")

		log.Println("Processing comments...")
		commentLinks = get_processed_comments(commentLinks, twitter_config.Trigger_words, twitter_config.Banned_words)
		log.Println("Done processing comments...")

		/* Get a bitly link and post to twitter */
		for _, comment_link := range commentLinks {
			/* Combine the youtube comment with the bitly link*/
			tweet_content := format_tweet(comment_link)
			/* Check if we've tweet this noise before, if so, don't tweet, MAX_RUDE */
			tweet_exists := db_config.CheckforTweet(tweet_content)
			if tweet_exists {
				continue
			}

			/* Tweet into the void! */
			tweeted := twitter_config.Tweet(tweet_content)

			/* If for some reason the tweet did not complete, try again! */
			if tweeted == false {
				continue
			}
			/* Record the tweet */
			db_config.AddTweet(tweet_content)

			post_frequency_range := twitter_config.Post_frequency_range
			if post_frequency_range == 0 {
				post_frequency_range = int32(60)
			}

			duration := time.Duration(twitter_config.Post_volume_in_sec+rand.Int31n(post_frequency_range)) * time.Second
			log.Println("Sleeping for [ ", duration, " ]")
			time.Sleep(duration)
		}
	}
}
