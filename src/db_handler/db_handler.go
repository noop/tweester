package db_handler

import (
	"fmt"
	sqlite3 "github.com/kuroneko/gosqlite3"
	"log"
)

type Db_handler struct {
	Db       *sqlite3.Database
	Filename string
}

func (db *Db_handler) Build() {
	/* Init the db handler */
	log.Println("Supplied Filename [ ", db.Filename, " ]")
	if db.Filename == "" {
		db.Filename = ":memory:"
	}
	db.Db, _ = sqlite3.Open(db.Filename)
	/* 
	* Select out of the table we expect to exist. If it doesn't, fall through and attempt
	* to create it.
	 */
	count, err := db.Db.Execute(" SELECT 1 FROM tweets LIMIT 1;")
	log.Println("Count [ ", count, " ] Error [ ", err, " ]")
	if count == 0 && err != nil {
		log.Println("Attempting to create the database ...")
		/* Create the database if it doesn't exist */
		count, err = db.Db.Execute("CREATE TABLE tweets (id INTEGER PRIMARY KEY ASC, tweet VARCHAR(140));")
		log.Println("Count [ ", count, " ] Error [ ", err, " ]")
		if err != nil {
			log.Fatal("Unable to create the desired table.")
		}
	}

}

/*
* Add a tweet to the database
 */
func (db *Db_handler) AddTweet(tweet string) (int, error) {
	statement := fmt.Sprintf("INSERT INTO tweets (tweet) VALUES ('%s');", tweet)
	count, err := db.Db.Execute(statement)
	db.Db.Commit()
	return count, err
}

/*
* Check to see if the tweet already exists
 */
func (db *Db_handler) CheckforTweet(tweet string) bool {
	statement := fmt.Sprintf("SELECT id FROM tweets WHERE tweet = '%s' LIMIT 1;", tweet)
	count, err := db.Db.Execute(statement)
	if count != 0 && err == nil {
		return true
	}
	return false
}

func blah_main() {
	var Db = new(Db_handler)
	Db.Filename = "angrybirds.db"
	Db.Build()
	/**
	* The SQLITE3 driver doesn't appear to support multi-value inserts. Oh well.
	* count, err := db.Execute( "INSERT INTO foo (name) VALUES ('HELLO'), ('FIRE THE MISSLES');" )
	 */
	tweet := "HELLO"
	if Db.CheckforTweet(tweet) == false {
		count, err := Db.AddTweet(tweet)
		if err != nil {
			log.Fatal("Insert failed, error [ ", err, " ]")
		} else {
			log.Println("INSERT success [ ", count, " ]")
		}
	} else {
		log.Println("Tweet [ ", tweet, " ] already exists!")
	}
	tweet = "FIRE THE MISSLES"
	if Db.CheckforTweet(tweet) == false {
		count, err := Db.AddTweet(tweet)
		if err != nil {
			log.Fatal("Insert failed, error [ ", err, " ]")
		} else {
			log.Println("INSERT success [ ", count, " ]")
		}
	} else {
		log.Println("Tweet [ ", tweet, " ] already exists!")
	}

	/*  Select out some rows from the future */
	all_rows := "SELECT * FROM tweets ORDER BY id ASC"
	s, err := Db.Db.Prepare(all_rows)
	if s != nil && err != nil {
		log.Fatal("Error selecting from foo, error [ ", err, " ]")
	}

	e := s.Step()
	for e != nil {
		values := s.Row()
		log.Println("Value [ ", values[1], " ]")
		e = s.Step()
	}
}
