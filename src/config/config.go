package config

import (
	"bitly"
	"db_handler"
	"encoding/json"
	"io"
	"log"
	"reddit"
	"twitter"
	"youtube"
)

/**
* A package to make dealing with config saner.
 */

type Config struct {
	/* Found in src/twitter/twitter.go */
	Twitter twitter.Twitter
	/* Found in src/youtube/youtube.go */
	Youtube youtube.Youtube
	/* Found in src/reddit/reddit.go */
	Reddit reddit.Reddit
	/* Found in src/bitly/bitly.go */
	Bitly bitly.Bitly
	/* Found in src/db_handler/db_handler.go */
	Db_handler db_handler.Db_handler
}

var config = new(Config)

func (c *Config) Build(data io.Reader) {
	/* With the provided json generate configurations */
	dec := json.NewDecoder(data)
	err := dec.Decode(&c)
	if err != nil {
		log.Fatal("Unable to map provided json file to config struct, aborting! Error [ ", err, " ]")
	}
	c.Bitly.Auth()
	c.Db_handler.Build()
}
