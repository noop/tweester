package generator

type Generator struct {
	T	string  `json:"$t"`
	Uri	string
	Version	string
}
