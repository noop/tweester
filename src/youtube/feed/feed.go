package feed

import (
	"youtube/author"
	"youtube/category"
	"youtube/generator"
	"youtube/id"
	"youtube/link"
	"youtube/logo"
	"youtube/entry"
)
type Feed struct {
	Author	    []author.Author
	Category    []category.Category
	Entry       []entry.Entry
	Generator   generator.Generator
	Id          id.Id
	Link        []link.Link
	Logo        logo.Logo
}
