package youtube

import (
	"youtube/feed"
	"youtube/generator"
	"youtube/logo"
	"youtube/os_itemsPerPage"
	"youtube/os_startIndex"
	"youtube/os_totalResults"
	"youtube/title"
	"youtube/updated"
)

/* A configuration file to hold twitter related info */
type Youtube struct {
	Base_url    string
	Video_ext   string
	Comment_ext string
}

type Response struct {
	Encoding               string
	Feed                   feed.Feed
	Generator              generator.Generator
	Logo                   logo.Logo
	OpenSearchItemsPerPage os_itemsperpage.OS_ItemsPerPage `json:"openSearch$itemsPerPage"`
	OpenSearchStartIndex   os_startindex.OS_StartIndex     `json:"openSearch$startIndex"`
	OpenSearchTotalResults os_totalResults.OS_totalResults `json:"openSearch$totalResults"`
	Title                  title.Title
	Updated                updated.Updated
	Xmlns                  string
	XmlnsApp               string
	XmlnsGd                string
	XmlnsGorss             string
	XmlnsGml               string
	XmlnsMedia             string
	XmlnsOpenSearch        string
	XmlnsYt                string
}

/* Make a twatter object */
var youtube = new(Youtube)
