package mediaContent

type MediaContent struct {
	Duration    int
	Expression  string
	//IsDefault   bool
	IsDefault   string
	Medium	    string
	Type	    string
	Url	    string
	YtFormat    int	    `json:"yt$format"`
}
