package entry

import (
	"youtube/author"
	"youtube/content"
	"youtube/category"
	"youtube/gdcomment"
	"youtube/id"
	"youtube/link"
	"youtube/mediaGroup"
	"youtube/published"
	"youtube/title"
	"youtube/updated"
	"youtube/recorded"
	"youtube/statistics"
)

type Entry struct {
    Author	    []author.Author
    Category	    []category.Category		    `json:"category"`
    Content	    content.Content
    GdComments	    gdcomment.GdComment		    `json:"gd$comments"`
    Id		    id.Id
    Link	    []link.Link
    MediaGroup	    mediaGroup.MediaGroup	    `json:"media$group"`
    Published	    published.Published
    Title	    title.Title
    Updated	    updated.Updated
    Recorded	    recorded.Recorded		    `json:"yt$recorded"`
    Statistics	    statistics.Statistics	    `json:"yt$statistics"`
}
