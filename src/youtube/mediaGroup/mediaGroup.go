package mediaGroup

import (
	"youtube/mediaCategory"
	"youtube/mediaContent"
	"youtube/mediaDescription"
	"youtube/mediaKeywords"
	"youtube/mediaPlayer"
	"youtube/mediaThumbnail"
	"youtube/mediaTitle"
	"youtube/duration"
	"youtube/published"
	"youtube/title"
	"youtube/updated"
	"youtube/recorded"
	"youtube/statistics"
)

type MediaGroup struct {
	MediaCategory	    []mediaCategory.MediaCategory	    `json:"media$group"`
	MediaContent	    []mediaContent.MediaContent		    `json:"media$content"`
	MediaDescription    mediaDescription.MediaDescription	    `json:"media$description"`
	MediaKeywords	    mediaKeywords.MediaKeywords		    `json:"media$keywords"`
	MediaPlayer	    []mediaPlayer.MediaPlayer		    `json:"media$player"`
	MediaThumbnail	    []mediaThumbnail.MediaThumbnail	    `json:"media$thumbnail"`
	MediaTitle	    mediaTitle.MediaTitle		    `json:"media$title"`
	YtDuration	    duration.Duration			    `json:"yt$duration"`
	Published	    published.Published
	Title		    title.Title
	Updated		    updated.Updated
	YtRecorded	    recorded.Recorded			    `json:"yt$recorded"`
	YtStatistics	    statistics.Statistics		    `json:"yt$statistics"`
}
