package category

type Category struct {
	Label	string	    `json:"label"`
	Scheme	string	    `json:"scheme"`
	Term	string	    `json:"term"`
}
