package mediaThumbnail

type MediaThumbnail struct {
	Height  int
	Time	string
	Url	string
	Width	int
}
