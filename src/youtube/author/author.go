package author

import (
	"youtube/name"
	"youtube/uri"
)

type Author struct {
	Name	name.Name
	Uri	uri.Uri
}
