package gdcomment

import (
	"youtube/gdfeedlink"
)

type GdComment struct {
	GdfeedLink  gdfeedlink.GdfeedLink	`json:"gd$feedLink"`
}
