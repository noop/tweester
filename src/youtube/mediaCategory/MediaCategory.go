package mediaCategory

type MediaCategory struct {
	t	string	    `json:"$t"`
	Label	string
	Scheme	string
}
