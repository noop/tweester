package link

type Link struct {
	Href	string
	Rel	string
	Type	string
}
