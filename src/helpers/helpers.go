package helpers

import (
	"encoding/json"
	"errors"
	"io"
	"strings"
	"unicode"
)

func Byte_parse_json(data []byte) (map[string]interface{}, error) {
	var body map[string]interface{}

	err := json.Unmarshal(data, &body)
	return body, err
}

func Io_parse_json(data io.Reader) (map[string]interface{}, error) {
	// Decode the configuration file
	dec := json.NewDecoder(data)

	// Declare a config variable
	var config map[string]interface{}

	// Decode the json into the config variable
	err := dec.Decode(&config)

	return config, err
}

func Addmap(a map[string]string, b map[string]string) map[string]string {
	var newMap map[string]string
	for k, v := range b {
		newMap[k] = v
	}
	for k, v := range a {
		newMap[k] = v
	}
	return newMap
}

/* Some comments are boring, let's skip them. */
func BoringComment(comment string, boring_words []string) (string, error) {
	// Check for banned words, if so throw an error
	if boring_words != nil {
		for _, word := range boring_words {
			if strings.Contains(comment, word) {
				return "", errors.New("boring word")
			}
		}
	}
	return comment, nil
}

/*  
*   Desparate attempt to find some kind of twitter hit
*   Mark any capitalized words with hashtags if count remains
 */
func CapitilizeNouns(comment string, hashtag_count int) string {
	var hashtagged_comment string
	count := hashtag_count
	for index, word := range strings.Fields(comment) {
		/* 
		* If we haven't hit three hashtags yet, the word
		* is uppercase and this isn't the first word of the comment
		 */
		if count < 3 && unicode.IsUpper(rune(word[0])) && index > 0 {
			hashtagged_comment += "#"
			count++
		}
		hashtagged_comment += string(word) + " "
	}
	return hashtagged_comment
}

/*  
*   Hashtag trigger words because we need to
 */
func MarkTriggers(comment string, trigger_words []string) string {
	var trigger_comment string
	if trigger_words != nil {
		var count int
		/* Toss these into a channel and chirp */
		for _, field := range strings.Fields(comment) {
			for _, word := range trigger_words {
				//log.Println("Testing trigger word [ ",field," ] against [ ",word," ]")
				if strings.ToLower(word) == strings.ToLower(field) && count < 3 {
					trigger_comment += "#"
					count++
				}
			}
			trigger_comment += field + " "
		}
	} else {
		trigger_comment = comment
	}
	return trigger_comment
}

func ProcessComment(comment string, trigger_words []string, boring_words []string) (string, error) {
	var comment_in_work string
	comment_in_work, err := BoringComment(comment, boring_words)
	if err == nil {
		comment_in_work = MarkTriggers(comment_in_work, trigger_words)
		count := strings.Count(comment_in_work, string('#'))
		/* If we've already got three words marked, skip this part as we've got enough hashtags*/
		if count < 3 {
			comment_in_work = CapitilizeNouns(comment_in_work, count)
		}
		return comment_in_work, err
	}
	return comment_in_work, err
}

/*
* A bulk comment processor
 */
func ProcessComments(comments []string, trigger_words []string, boring_words []string) ([]string, error) {
	var markedComments []string
	for _, comment := range comments {
		polished_comment, err := ProcessComment(comment, trigger_words, boring_words)
		if err == nil {
			markedComments = append(markedComments, polished_comment)
		}
	}
	return markedComments, nil
}
