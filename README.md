Tweester is a cross posting bot from YouTube to Twitter.  

Why?  

Why not? It's an art project in communication.  

Requirements  

sqlite3  
github.com/ChimeraCoder/anaconda -- twitter oauth/api lib  

Build  

yum/apt-get install sqlite3  
go get github.com/ChimeraCoder/anaconda  

go build src/tweester/tweester.go  

Copy the config/config.json.example to your desired configuration location.  
Edit the config.json with auth information.  

Invocation  
./bin/tweets --config /path/to/config.json  
